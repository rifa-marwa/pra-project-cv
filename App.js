import React, { useState } from "react";
import Home from "./screens/home";

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  return <Home />;
}
